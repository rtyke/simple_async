# Simple Async Server

Simple asynchronous HTTP-server. Accept search queries and return themes corresponding to these querires.

#### Example usage

Send search query as query params of GET request

```
curl -iG http://localhost:8000/search --data-urlencode "text=рецепт вкусного борща"
```
Response
```
{'themes': ['кухня']}
```

If you see codepoints try this
```
echo `curl -iG http://localhost:8001/search --data-urlencode "text=рецепт вкусного борща"`
```

 

#### Requirements:
python 3.6 or older

#### Installation

[Sanic](https://github.com/huge-success/sanic) package will be installed during Simple Async Server Installation so I recommend activate virtualenv before installation.
``` 
virtuelaenv --no-site-packages -p python3 env_serv
source env_serv/bin/activate

```

Install

```
git clone git@gitlab.com:rtyke/simple_async.git
cd simple_async
python setup.py install
```
#### Run
Run server on port 8000
```
runserv
```
Run server in port 8002
```
runserv 8002
```
