import sys

from sanic import Sanic, response
from signal import signal, SIGINT
import asyncio
import uvloop


THEME_CATALOG = {
    "новости": (
        ("деревья", "на", "cадовом", "кольце"),
        ("добрый", "автобус"),
        ("выставка",  "it", "технологий"),
    ),
    "кухня": (
        ("рецепт", "борща"),
        ("яблочный", "пирог"),
        ("тайская", "кухня"),
    ),
    "товары": (
        ("дети", "капитана", "гранта"),
        ("зимние", "шины"),
        ("тайская", "кухня"),
    ),
}


app = Sanic(__name__)


def match_theme(user_query, theme_catalog):

    matched_themes = []

    user_query_normalized = user_query.lower()

    for theme, phrases in theme_catalog.items():

        for phrase in phrases:

            if all(word in user_query_normalized for word in phrase):
                matched_themes.append(theme)
                break

    return matched_themes


@app.get('/search')
async def get_handler(request):
    try:
        user_query = request.args['text'][0]
    except (TypeError, KeyError, IndexError):
        return response.json(
            {'error': 'wrong query params'},
            status=400,
        )

    matched_themes = match_theme(user_query, THEME_CATALOG)

    return response.json(
        {"matched_themes": matched_themes},
        status=200,
        content_type="application/json; charset=utf-8",
    )


def main():
    try:
        port_number = int(sys.argv[1])
    except (IndexError, ValueError):
        port_number = 8000

    asyncio.set_event_loop(uvloop.new_event_loop())

    server = app.create_server(host="0.0.0.0", port=port_number, return_asyncio_server=True)

    loop = asyncio.get_event_loop()
    task = asyncio.ensure_future(server)

    signal(SIGINT, lambda s, f: loop.stop())

    try:
        loop.run_forever()
    except:
        loop.stop()


if __name__ == "__main__":
    main()
